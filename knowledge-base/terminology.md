# Game Terminology
In the game there are used many terms and abbreviations. Here's the list of (some of) them

## General
- alt - another character
- asscard - The ship cards used for admiralty assignments
- boff - Bridge Officer
- craptic - A mixture of words `crap` and `cryptic`. Go to our #craptic discord channel and you'll know what it means
- doff - Duty Officer
- maynard - To transfer resources from one alt to another in order to improve your speed of development. Usually done when you start a new alt, you maynard some doffs here. Can be done via mail to yourself or by an account bank. 
- q'd - The team has joined a queue 
- toon - character

## Currency
- dil - Dilithium
- EC - Energy Credits
- FC - Fleet Credits

## Boff abilities
### Tactical
- APB - Attack Pattern Beta
- APD - Attack Pattern Delta
- APO - Attack Pattern Omega
- BO - Beam Overload
- CSV - Cannon Scatter Volley
- CRF - Cannon Rapid Fire
- FAW - Fire At Will
- kemo - Kemocite-Laced Weaponry
- THY - Torpedoes: High Yield
- TS - Torpedoes: Spread
- TT - Tactical Team

### Engineering
- ATB / A2B - Auxiliary Power to Emergency Battery
- DEM - Directed Energy Modulation
- EPTA - Emergency Power to Auxiliary
- EPTE - Emergency Power to Engines
- EPTS - Emergency Power to Shields
- EPTW - Emergency Power to Weapons
- EPTX - Any of EPTW, EPTS, EPTE, EPTA
- ET - Engineering Team
- RSP - Reverse Shield Polarity

### Science
- GW - Gravity Well
- HE - Hazard Emitters
- FBP - Feedback Pulse
- PH - Polarize Hull
- pulling TBR - TBR enhanced by a doff to pull targets towards the caster instead of pushing them away
- ST - Science Team
- TaB - Tachyon Beam
- TBR - Tractor Beam Repulsors
- TrB - Tractor Beam
- TSS - Transfer Shield Strength

## Characters
### Species
- free'hadar / freehadar - The Jem'hadar that aren't payhadars (referring to player race)
- pay'hadar / payhadar / zenhadar - Jem'hadar Vanguard

## Items
- colony supplies - a common term for fleet colony luxuries, batteries and ore
- shitbox - lockbox

### Weapons
- DBB - Dual Beam Bank
- DHC - Dual Heavy Cannons
- torp - torpedo

## Ships
- HEC - Heavy Escort Carrier
- JHD / JHDC - Jem'hadar Dreadnought Carrier

## Traits
### Ground

### Space

### Starship
- IGW - Improved Gravity Well