# Some random tips
- Use admiralty to level and earn specialty points past L52
- Use a powerboard (Risa summer event) on Colony world to move quickly between fleet colony supplies gathering locations
- For leveling use Escort type ships (kill faster, level faster), if you don't own any better ships only the L40/L61 picks really matters
- You can send a mail to yourself. It's one of ways to perform maynard. You can also drag & drop doffs there alongside with items.
- You can take off some non-bound doffs into mail for a future use
